"user strict"; 

const coreCtrl = require('../controller').Core;

module.exports = function(app) {
    app.route('/').get(coreCtrl.renderHomePage);
    app.route('/contact').get(coreCtrl.renderContactPage);
};
